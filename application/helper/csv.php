<?php
class helper_csv
{
	/**
	 * 导出数据转换
	 *
	 * @param $result <type>
	 */
	static public function byArray(array $result,$content = '')
	{
		$content = $content."\r\n";
		foreach ($result as $key => $value)
		{
			$row = '';
			if (is_array($value)) {
				foreach ($value as $key2 => $value2)
				{
					$row .= ','.$value2;
				}
			}
			else {
				$row = $value;
			}
			$content .= trim($row,',')."\r\n";
		}
		return $content;
	}
}