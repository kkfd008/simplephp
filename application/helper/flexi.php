<?php
class helper_flexi
{
	private $logined = false;
	private $tempnam = '/tmp/flexi';
	private $cookie;
	private $host;
	
	public function getLogined()
	{
		return $this->logined;
	}
	
	public function __construct()
	{
		$this->host = 'http://192.168.0.160:89';
// 		$this->account = 'anthony.acs8@gmail.com';
// 		$this->pass = 'nbiezl';
	}
	
	public function logon($account, $pass)
	{
		$url = $this->host . '/logon';
		$querydata = array(
			'101' => $account,
			'102' => $pass,
			'submit' => 'Logon',
			'submitdef' => 'Logon',
			'onok' => 'main',
			'oncancel' => 'main'
		);
		
		$ch = curl_init( $url );
		if ( ! is_writable( $this->tempnam ) )
		{
			mkdir( $this->tempnam, 0777 );
		}
		
		$this->cookie = tempnam( $this->tempnam, "flexiserver_" );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_HEADER, true );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 120 );
		curl_setopt( $ch, CURLOPT_POST, true );
		curl_setopt( $ch, CURLOPT_COOKIESESSION, true );
		curl_setopt( $ch, CURLOPT_COOKIEJAR, $this->cookie );
		curl_setopt( $ch, CURLOPT_POST, count( $querydata ) );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $querydata ) );
		$result = curl_exec( $ch );
		curl_close( $ch );
// 		var_dump($result);
		if ( false === strpos( $result, 'Location: mainpage' ) )
		{
			$this->logined = false;
			// die( $account . 'flexi login failer' );
			return $this;
		}
		$this->logined = true;
		return $this;
	}
	
	private function cookiecurl($url, array $querydata = array())
	{
		$ch = curl_init( $url );
		curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 120 );
		
		curl_setopt( $ch, CURLOPT_COOKIEFILE, $this->cookie );
		curl_setopt( $ch, CURLOPT_COOKIEJAR, $this->cookie );
		if ( 0 < sizeof( $querydata ) )
		{
			curl_setopt( $ch, CURLOPT_POST, true );
			curl_setopt( $ch, CURLOPT_POST, count( $querydata ) );
			curl_setopt( $ch, CURLOPT_POSTFIELDS, http_build_query( $querydata ) );
		}
		$result = curl_exec( $ch );
		curl_close( $ch );
		return $result;
	}
	
	public function viewhours($seluser, $startdate, $enddate)
	{
		if ( false == $this->getLogined() ) return false;
		
		// viewhours?post=true&seluser=anthony.acs8@gmail.com&startdate=2012-12-01&enddate=2012-12-16
		$url = $this->host . '/viewhours?post=true';
		$querydata = array(
			'seluser' => $seluser,
			'startdate' => $startdate,
			'enddate' => $enddate,
			'daterange' => 0,
			'submit' => 'Search'
		);
		
		$result = $this->cookiecurl( $url, $querydata );
		$pattern = '/<tr class=tr([0|1])>([\w\W]*?)<\/tr>/i';
		preg_match_all( $pattern, $result, $trdata );
		foreach ( $trdata[2] as $key => $value )
		{
			$mydata = $tddata = array();
			$pattern = '/<td align=left valign=middle class=hltd>([\w\W]*?)<\/td>/i';
			preg_match_all( $pattern, $value, $tddata );
			
			foreach ( $tddata[1] as $key => $value )
			{
				$mydata[$key] = trim( strip_tags( $value ), '&nbsp;' );
			}
			
			$data[ ] = $mydata;
		}
		return $data;
	}
	
	public function timesheet($seluser, $date)
	{
		if ( false == $this->getLogined() ) return false;
		
		$url = $this->host . '/timesheet?＝=' . $seluser . '&date=' . $date;
		$url .= '&starttime=00:00:00&endtime=24:00:00';
		$result = $this->cookiecurl( $url );
		
		$pattern = '/<tr class=tr([0|1])>([\w\W]*?)<\/tr>/i';
		preg_match_all( $pattern, $result, $trdata );
		
		foreach ( $trdata[0] as $key => $value )
		{
			if ( 0 < strpos( $value, 'active' ) ) $key = 'active';
			if ( 0 < strpos( $value, 'idle' ) ) $key = 'idle';
			if ( 0 < strpos( $value, 'break' ) ) $key = 'break';
			$pattern = '/<div align=left>([\w\W]*?)<\/div>/i';
			
			preg_match_all( $pattern, $value, $divdata );
			$data[ ] = $divdata[1];
		}
		
		return $data;
	}
	
	public function viewscreens($seluser, $date)
	{
		if ( false == $this->getLogined() ) return false;
		$data = array();
		// viewscreens?seluser=anthony.acs8@gmail.com&date=2012-11-08
		$date = date( 'Y-m-d' );
		$url = $this->host . '/viewscreens?seluser=' . $seluser . '&date=' . $date;
		$result = $this->cookiecurl( $url );
		
		$pattern = '/<img src=\"getimg.jpg\?([\w\W]*?)\" title=\"Click to view full size image\" alt=\"Click to view full size image\" border=0>/i';
		preg_match_all( $pattern, $result, $imgdata );
		$tmpAry = array_reverse( $imgdata[1] );
		foreach ( $tmpAry as $value )
		{
			$pattern = '/seluser=([^&]+)\&date=([0-9-]+)\&time=([0-9:]+)/i';
			preg_match_all( $pattern, $value, $valuedata );
			$data[ ] = array(
				'seluser' => $valuedata[1][0],
				'date' => $valuedata[2][0],
				'time' => $valuedata[3][0]
			);
		}
		return $data;
	}
	
	public function getimg($seluser, $date, $time)
	{
		if ( false == $this->getLogined() ) return false;
		
		// getimg?seluser=anthony.acs8@gmail.com&
		$url = $this->host . '/getimg.jpg?seluser=' . $seluser . '&date=' . $date . '&time=' . $time;
		$result = $this->cookiecurl( $url );
		return $result;
	}
	
	public function fullimg($seluser, $date, $time)
	{
		if ( false == $this->getLogined() ) return false;
		
		// fullsizeimg?seluser=anthony.acs8@gmail.com&date=2012-12-13&time=08:32:11
		$url = $this->host . '/fullsizeimg.jpg?seluser=' . $seluser . '&date=' . $date . '&time=' . $time;
		$result = $this->cookiecurl( $url );
		return $result;
	
	}

}