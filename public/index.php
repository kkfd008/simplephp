<?php
// nv init
error_reporting( E_ALL^E_NOTICE );
date_default_timezone_set( 'Asia/Shanghai' );

ini_set('session.cookie_path', '/');
session_start();

// ust define
if ( 5.2 < substr( PHP_VERSION, 0, 3 ) )
{
	define( 'PROJECT_DOCUMENT_ROOT', realpath( dirname( __DIR__ ) ) );
}
else
{
	define( 'PROJECT_DOCUMENT_ROOT', realpath( dirname( __FILE__ ) . '/../' ) );
}

define( 'PROJECT_APPLICATION_CONFIG', PROJECT_DOCUMENT_ROOT . '/config/development.php' );
define( 'PROJECT_LIBRARIES_SIMPLEPHP', PROJECT_DOCUMENT_ROOT . '/libraries/simplephp' );

define( 'FRAMEWORK_SIMPLEPHP_MONITOR', false );

// mport bootstartp.php
$bootstrap = PROJECT_LIBRARIES_SIMPLEPHP . "/bootstrap.php";
require_once ($bootstrap);

// un
class myApp extends sp_bootstrap implements sp_applicaiton
{
	public function run()
	{
		parent::run( );
	}
}

$myApp = new myApp( );
$myApp->run( );