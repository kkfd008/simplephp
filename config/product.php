<?php
return array(
	'domain' => array(
		'title' => 'this is simple php framework',
		'url' => '/public'
	),
	'plugin_template_smarty' => array(
		'template_dir' => PROJECT_DOCUMENT_ROOT . '/theme',
		'compile_dir' => '/tmp',
		'config_dir' => null,
		'cache_dir' => null,
		'plugins_dir' => PROJECT_DOCUMENT_ROOT . '/theme/plugins',
		'left_delimiter' => '<!--{',
		'right_delimiter' => '}-->'
	),
);