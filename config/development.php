<?php
return array_merge( include ('product.php'), array(
	'domain' => array(
		'title' => 'freelance_hr_payroll for development',
		'url' => '/freelancer_hr_payroll/public'
	),
	'extends_db_mysql' => array(
		'freelancer' => array(
			'username' => 'root',
			'password' => 'rootmysql',
			'host' => 'localhost',
			'port' => 3306
		),
		'anshechu_anshex' => array(
			'username' => 'root',
			'password' => 'anshex123',
			'host' => '192.168.0.77',
			'port' => 3306
		)
	),
	'extends_db_mongo' => array(
		'freelance' => array(
			'username' => null,
			'password' => null,
			'host' => 'localhost',
			'port' => 27017
		)
	),
) );