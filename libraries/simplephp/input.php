<?php
if (!defined('FRAMEWORK_SIMPLEPHP_VERSION')) exit('framework not setting,cannot load'.__FILE__);
class sp_input
{
	public function __set($property, $value)
	{
		$this->$property = $value;
	}
	
	public function __get($property)
	{
		if ( property_exists( $this, $property ) )
		{

		}
		else
		{
			return FALSE;
		}
	}
	
	public function filter($property, $flag, $option = FALSE)
	{
		// validate/sanitize/unsafe/callback
		// 改变$property的值
		$this->$property = filter_var( $this->$property, $flag, $option );
		return $this;
	}
	
	public function get($property, $define)
	{
		if ( FALSE == $this->$property )
		{
			return $define;
		}
		return $this->$property;
	}
}
