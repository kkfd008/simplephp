<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );

class sp_response_template
{
	private $processhandler;
	
	private $assign = array();
	
	public function setAssign($key, $value)
	{
		$this->assign[$key] = $value;
	}
	
	public function render($template, array $option = array(), $cache = null)
	{
		foreach ($option as $key => $value)
		{
			${$key} = $value;
		}
		
		foreach ($this->assign as $key => $value)
		{
			${$key} = $value;
		}
		//var_dump($this);
		$_templatePath = FRAMEWORK_TEMPLATE_PATH;
		$_templatefile = $_templatePath.$template;
		
		if (file_exists($_templatefile)) {
			ob_start();
			include $_templatefile;
			$_renderResult = ob_get_contents();
			ob_end_clean();
			return $_renderResult;
		}
		return null;
	}
}

?>