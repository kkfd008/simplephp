<?php
if (!defined('FRAMEWORK_SIMPLEPHP_VERSION')) exit('framework not setting,cannot load'.__FILE__);
abstract class sp_stdclass
{
	public function __get($property)
	{
		if ( ! property_exists( $this, $property ) ) throw new Exception( __CLASS__ . "no exist $property" );
		$_method = 'set' . ucwords( $property );
		if ( method_exists( $this, $_method ) )
		{
			return $this->$_method( $property );
		}
		else
		{
			return $this->$property;
		}
	}
	
	public function __set($property, $value)
	{
		if ( ! property_exists( $this, $property ) ) throw new Exception( __CLASS__ . "no exist $property" );
		$_method = 'set' . ucwords( $property );
		if ( method_exists( $this, $_method ) )
		{
			return $this->$_method( $property );
		}
		else
		{
			$this->$property = $value;
			return $this;
		}
	}
}

?>