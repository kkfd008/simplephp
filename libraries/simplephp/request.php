<?php
if ( ! defined( 'FRAMEWORK_SIMPLEPHP_VERSION' ) ) exit( 'framework not setting,cannot load' . __FILE__ );
class sp_request
{
	private $cookie;
	private $get;
	private $post;
	private $request;
	
	private $env;
	private $files;
	private $server;
	private $session;
	
	public function __get($property)
	{
		if ( property_exists( $this, $property ) && is_object($this->$property))
		{
			return $this->$property;
		}
		else
		{
			throw new Exception( __CLASS__ . " no property $property exist" );
		}
	}
	
	public function __set($property,$value)
	{
		$this->$property = $value;
		return $this;
	}
	
	public function __construct()
	{
		$this->cookie = new sp_request_cookie();
		$this->env = new sp_request_env();
		$this->files = new sp_request_files();;
		$this->get = new sp_request_get();
		$this->post = new sp_request_post();
		$this->request = new sp_request_request();
		$this->server = new sp_request_server();
		$this->session = new sp_request_session();
	}
}


?>