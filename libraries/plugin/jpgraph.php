<?php
class plugin_jpgraph
{
	static public function getGraph()
	{
		require_once ('jpgraph/jpgraph.php');
	}
	
	static public function getLine()
	{
		require_once ('../jpgraph/jpgraph_line.php');
	}
	
	static public function getBar()
	{
		require_once ('../jpgraph/jpgraph_bar.php');
	}
}

?>