<?php
interface extends_db_interface
{
	//public function __construct($database, $host = null, $port = null, $username = null, $password = null);
	public function from($table);
	public function count(array $value = array());
	public function filter(array $condition = array(), $init = false);
	public function set(array $data = array(), $init = false);
	public function sort(array $condition = array());
	public function limit($value);
	public function offset($value);
	public function save();
	public function fetch();
	public function del();
}

?>