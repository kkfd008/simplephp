<?php
class extends_db_mysql_parse extends extends_db_parse
{
	// public function pagesize($size, $page = 1)
	// {
	// $this->offset(0);
	// if (is_int($page) && $page > 1) $this->offset(($page-1)*$size);
	// $this->limit($size);
	// }
	
	protected function parseWhere()
	{
		$parseWhere = array();
		if ( 0 >= sizeof( $this->condition ) ) return null;
		foreach ( $this->condition as $key => $value )
		{
			if ( is_array( $value ) && is_numeric( $key ) ) $parseWhere[ ] = '(' . join( ' OR ', $value ) . ')';
			elseif ( !is_array( $value ) && is_string( $key ) ) $parseWhere[ ] = ' `' . $key . '` = "' . $value . '"';
			elseif ( !is_array( $value ) && ! is_string( $key ) ) $parseWhere[ ] = $value;
			
		}
		if ( 0 < sizeof( $parseWhere ) ) return ' WHERE ' . trim( join( ' AND ', $parseWhere ) );
		return NULL;
	}
	
	protected function parseSet()
	{
		if ( 0 >= sizeof( $this->data ) ) return null;
		$parseSet = array();
		foreach ( $this->data as $key => $value )
		{
			if ( is_array( $value ) && is_string( $key ) ) $parseSet[ ] = ' `' . $key . '` = ' . trim( join( ',', $value ), ',' );
			elseif ( ! is_array( $value ) && is_string( $key ) ) $parseSet[ ] = ' `' . $key . '` = "' . $value . '"';
		}
		if ( 0 < sizeof( $parseSet ) ) return ' SET ' . trim( join( ',', $parseSet ) );
		return NULL;
	}
	
	protected function parseOrder()
	{
		if ( 0 >= sizeof( $this->sort ) ) return null;
		foreach ( $this->sort as $key => $value )
		{
			// $set = '`'.$key.'` = \''.$value .'';
			$parseSet .= ",`$key` $value";
		}
		return ' ORDER BY ' . trim( $parseSet, ',' );
	}
	
	protected function parseLimit()
	{
		$parseLimit = '';
		if ( $this->offset && $this->limit )
		{
			$parseLimit = " LIMIT $this->offset,$this->limit";
		}
		elseif ( ! $this->offset && $this->limit )
		{
			$parseLimit = " LIMIT 0,$this->limit";
		}
		else
		{
			$parseLimit = " LIMIT 0,20";
		}
		return $parseLimit;
	}
}

