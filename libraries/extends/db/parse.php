<?php
abstract class extends_db_parse
{
	protected $count = array();
	protected $data = array();
	protected $condition = array();
	protected $sort = array();
	protected $limit = NULL;
	protected $offset = NULL;
	
	protected function clear()
	{
		$this->count = array();
		$this->data = array();
		$this->condition = array();
		$this->sort = array();
		$this->limit = NULL;
		$this->offset = NULL;
	}
	
	public function count(array $value = array())
	{
		foreach ( $value as $key => $value )
		{
			$this->count[$key] = $value;
		}
		return $this;
	}
	
	public function filter(array $condition = array(), $init = false)
	{
		if ( true == $init ) $this->condition = array();
		foreach ( $condition as $key => $value )
		{
			$this->condition[$key] = $value;
		}
		return $this;
	}
	
	public function set(array $data = array(), $init = false)
	{
		if ( true == $init ) $this->data = array();
		foreach ( $data as $key => $value )
		{
			$this->data[$key] = $value;
		}
		return $this;
	}
	
	public function sort(array $orderby = array(), $init = false)
	{
		if ( true == $init ) $this->sort = array();
		foreach ( $orderby as $key => $value )
		{
			$this->sort[$key] = $value;
		}
		return $this;
	}
	
	public function limit($value)
	{
		$this->limit = $value;
		return $this;
	}
	
	public function offset($value)
	{
		$this->offset = $value;
		return $this;
	}
}