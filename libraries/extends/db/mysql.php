<?php
/**
 * INSERT INTO `tab` set `col1`=a1,`col2`=a2,`col3`=a3;  <=>  INSERT INTO tab(`col1`,`col2`,`col3`) values(a1,a2,a3);
 * INSERT INTO `tab`(`col1`,`col2`,`col3`) values(a1,a2,a3),(b1,b2,b3),(c1,c2,c3);
 * DELETE FROM `tab` WHERE `col1` = a1 LIMIT 0,20;
 * UPDATE `tab` SET `col1` = a1 WHERE `col2` = a2;
 * SELECT `col1`,`col2` FROM `tab` WHERE ... ORDER BY ... LIMIT ...
 * @author qinqin
 *
 */
class extends_db_mysql extends extends_db_mysql_parse implements extends_db_interface
{
	protected $link;
	protected $db;
	protected $collection;
	protected $cursor;
	protected $filed = '*';
	
	/*
	 * (non-PHPdoc) @see extends_db_interface::__construct()
	 */
	public function __construct($database, $host = 'localhost', $port = '3306', $user = null, $pass = null)
	{
		// TODO Auto-generated method stub
		$this->link = mysql_connect( $host . ':' . $port, $user, $pass, true ) or die( 'extends_db_mysql : errorno ' . mysql_errno( ) . ": " . mysql_error( ) );
		mysql_set_charset( 'utf8', $this->link ) or die( 'extends_db_mysql : errorno ' . mysql_errno( ) . ": " . mysql_error( ) );
		mysql_select_db( $database, $this->link ) or die( 'extends_db_mysql : errorno ' . mysql_errno( ) . ": " . mysql_error( ) );
		$this->conn = true;
	
	}
	
	/*
	 * (non-PHPdoc) @see extends_db_interface::__destruct()
	 */
	public function __destruct()
	{
		if ( is_resource( $this->link ) )
		{
			mysql_close( $this->link );
		}
	}
	
	/*
	 * (non-PHPdoc) @see extends_db_interface::from()
	 */
	public function from($table)
	{
		// TODO Auto-generated method stub
		$this->collection = $table;
	}
	
	private function query($querystring)
	{
		$this->clear();
		sp_log::writeline( $querystring );
		$result = mysql_query( $querystring, $this->link ) or die( 'extends_db_mysql errorno' . mysql_errno( ) . ": " . mysql_error( ) );
		return $result;
	}
	
	/*
	 * (non-PHPdoc) @see extends_db_interface::save()
	 */
	public function save()
	{
		// TODO Auto-generated method stub
		if ( $this->condition )
		{
			// generate mysql update string
			$query = 'UPDATE `' . $this->collection . '` ' . $this->parseSet( ) . $this->parseWhere( );
			$this->query( $query );
			return mysql_affected_rows( $this->link );
		}
		else
		{
			// generate mysql insert string
			$query = 'INSERT INTO ' . $this->collection . $this->parseSet( );
			$this->query( $query );
			return mysql_insert_id( $this->link );
		}
	}
	
	/*
	 * (non-PHPdoc) @see extends_db_interface::fetch()
	 */
	public function fetch()
	{
		// TODO Auto-generated method stub
		$result = array();
		if ( $this->count )
		{
			$query = 'SELECT ' . $this->count . ' FROM `' . $this->collection . '`' . $this->parseWhere( );
		}
		else
		{
			$query = 'SELECT ' . $this->filed . ' FROM `' . $this->collection . '`' . $this->parseWhere( ) . $this->parseOrder( ) . $this->parseLimit( );
		
		}
		$this->cursor = $this->query( $query );
		if ( is_resource( $this->cursor ) )
		{
			while ( $_row = mysql_fetch_array( $this->cursor, MYSQL_ASSOC ) )
			{
				$result[ ] = $_row;
			}
			mysql_free_result( $this->cursor );
			return $result;
		}
		return $result;
	}
	
	/*
	 * (non-PHPdoc) @see extends_db_interface::remove()
	 */
	public function del()
	{
		// TODO Auto-generated method stub
		$query = 'SELECT ' . $this->collection . $this->parseSet( ) . $this->parseWhere( );
		$this->query( $query );
		return mysql_affected_rows( $this->link );
	}


}