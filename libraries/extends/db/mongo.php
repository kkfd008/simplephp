<?php
class extends_db_mongo extends extends_db_mongo_parse implements extends_db_interface
{
	protected $link;
	protected $db;
	protected $collection;
	protected $cursor;
	
	public function __construct($database, $host = null, $port = null, $username = null, $password = null)
	{
		// mongodb://[username:password@]host1[:port1][,host2[:port2],
		
		if ( ! class_exists( 'Mongo' ) ) return null;
		$account = null;
		if ( null == $host ) $host = 'localhost';
		if ( null == $port ) $port = '27017';
		if ( null != $username && null != $password ) $account = "$username:$password@";
		$connstr = "mongodb://$account$host:$port";
		$this->link = new Mongo( $connstr );
		if ( ! class_exists( 'MongoDB' ) || ! $this->link instanceof Mongo ) return null;
		// $this->db = $this->mongo->selectDB( $this->database );
		$this->db = new MongoDB( $this->link, $database );
		return true;
	}
	
	public function __destruct()
	{
		//$this->link->close( );
	}
	
	public function from($table)
	{
		if ( ! class_exists( 'MongoCollection' ) || ! $this->db instanceof MongoDB ) return null;
		// $this->collection = $this->db->selectCollection( $table );
		$this->collection = new MongoCollection( $this->db, $table );
		return $this;
	}
	
	public function save()
	{
		if ( $this->condition )
		{
			$this->cursor = $this->collection->update( $this->condition, $this->data );
		}
		else
		{
			$this->cursor = $this->collection->insert( $this->data );
		}
		$this->clear();
		//todo  it dont finish return value
		return $this;
	}
	
	public function fetch()
	{
		$this->cursor = NULL;
		if ( $this->count )
		{
			return $this->collection->find( $this->count )->count( );
		}
		$this->cursor = $this->collection->find( $this->condition );
		if ( $this->offset ) $this->cursor = $this->collection->skip( $this->offset );
		if ( $this->limit ) $this->cursor = $this->collection->limit( $this->limit );
		$this->clear();
		return iterator_to_array($this->cursor);
	}
	
	public function del()
	{
		$this->collection->remove( $this->condition );
		$this->clear();
		return true;
	}

}